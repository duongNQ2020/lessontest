package com.nac.buoi8_5_4_2020;

import android.app.Application;

public class App extends Application {
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    private  String userName, passWord;
    private boolean resultRegister;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public void setResultRegister(boolean resultRegister) {
        this.resultRegister = resultRegister;
    }

    public boolean getResiltResgiter(){
        return resultRegister;
    }
}
