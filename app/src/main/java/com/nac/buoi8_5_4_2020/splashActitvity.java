package com.nac.buoi8_5_4_2020;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;

public class splashActitvity extends BaseActivity {


    @Override
    protected void initView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doTaskdelay();
            }


        }, 2000  );
    }

    private void doTaskdelay() {
        startActivity(new Intent(this, login.class));
        finish();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.splash;
    }
}
