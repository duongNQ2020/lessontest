package com.nac.buoi8_5_4_2020;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class actRegister extends BaseActivity {

    private EditText edt_us, edt_pass, edt_re_pass;





    @Override
    protected void initView() {
        edt_us = findViewById(R.id.edt_re_user_name);
        edt_pass = findViewById(R.id.edt_re_pass_word);
        edt_re_pass = findViewById(R.id.edt_re_re_pass);
        findViewById(R.id.bt_re_register).setOnClickListener(this);
        initData();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.register;
    }


    private void initData() {
        edt_us.setText(App.getInstance().getUserName());
        edt_pass.setText(App.getInstance().getPassWord());
        findViewById(R.id.ib_back,this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.bt_re_register){
            if(textOf(edt_us).isEmpty()
                    || edt_pass.getText().toString().isEmpty()
                    || edt_re_pass.getText().toString().isEmpty()){
                Toast.makeText(this,"Du lieu trong",Toast.LENGTH_LONG).show();
                return;
            }

            if(!edt_pass.getText().toString().equals(edt_re_pass.getText().toString())){
                Toast.makeText(this,"Ko khop password",Toast.LENGTH_LONG).show();
                return;
            }

            App.getInstance().setResultRegister(true);
            finish();
        }
        else if(v.getId() == R.id.ib_back){
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(edt_re_pass.getText().toString().isEmpty()){
            App.getInstance().setResultRegister(false);
            finish();
        }
    }
}
