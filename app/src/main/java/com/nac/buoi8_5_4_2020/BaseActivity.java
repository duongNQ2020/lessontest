package com.nac.buoi8_5_4_2020;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract  class BaseActivity extends AppCompatActivity implements View.OnClickListener{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        initView();
    }

    protected   abstract  void  initView();
    protected abstract int getLayoutId();


    public <T extends View> T findViewById(int id, View.OnClickListener event) {
        T v = findViewById(id);
        if (v != null && event != null) {
            v.setOnClickListener(event);
        }
        return v;
    }

    @Override
    public void onClick(View v) {
        //do nothing
    }

    public String textOf(TextView v){
        if(v!= null) return  null;
        return v.getText().toString();
    }


}
