package com.nac.buoi8_5_4_2020;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class login extends BaseActivity {


    private static final String TAG = login.class.getSimpleName();;
    private EditText edt_user, edt_pass;
    private boolean exit = false;



    @Override
    protected void initView() {
        edt_user = (EditText) findViewById(R.id.edt_user_name);
        edt_pass = (EditText) findViewById(R.id.edt_pass_word);
        findViewById(R.id.bt_register,this);
        findViewById(R.id.iv_exit,this);


    }
    @Override
    protected int getLayoutId() {
        return R.layout.login;
    }


    @Override
    public void onClick(View v) {
        if(v.getId() ==  R.id.bt_register) {
            if (edt_pass.getText().toString().isEmpty() || edt_user.getText().toString().isEmpty()) {
                Toast.makeText(this, "Du lieu trong!", Toast.LENGTH_LONG).show();
                return;
            }
            Log.i(TAG,edt_pass.getText().toString() instanceof String? "true":"false");
           App.getInstance().setUserName(edt_user.getText().toString());
            App.getInstance().setPassWord(edt_pass.getText().toString());
            startActivity(new Intent(this, actRegister.class));
        }
        else if(v.getId() == R.id.iv_exit){
            delayExit();
        }
    }

    private void delayExit() {
        if(exit){
            finish();
            return;
        }

        exit = true;
        Toast.makeText(this,"Nhan them 1 lan nua de ket thuc",Toast.LENGTH_LONG).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                exit =  false;
            }
        },1000);
    }

    @Override
    protected void onStart() {
        super.onStart();

        boolean resultResigter = App.getInstance().getResiltResgiter();
        if(resultResigter == true){
            Toast.makeText(this,"Dang ky thanh cong", Toast.LENGTH_LONG).show();

        }
        else Toast.makeText(this,"Dang ky that bai", Toast.LENGTH_LONG).show();

    }
}
